## Readme

Para rodar o projeto execute:

### `npm install`

Instala os pacotes necess rios para rodar o projeto;

### `npm start`

Executa tanto a api quanto o front-end;

api: http://localhost:3333
front: http://localhost:3000

### `npm run build`

Gera os arquivos est ticos na pasta /dist
