const fetchProducts = async () => {
  const res = await fetch(`http://localhost:3333/products`);
  if (res.ok) {
    return await res.json();
  }

  return Promise.reject();
}

export default fetchProducts;
