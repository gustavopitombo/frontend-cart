import React, { createContext, useContext, useReducer } from 'react';
import reducer from './reducer';

const getInitialState = () => {

  const initialState = { products: {}, total: 0, cartOpen: false };
  const localProducts = JSON.parse(localStorage.getItem( 'products' ));
  const selectedProducts = localProducts || initialState;

  return selectedProducts;
};

const CartContext = createContext();

export const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, getInitialState());

  return (
    <CartContext.Provider value={{ state, dispatch }}>
      {children}
    </CartContext.Provider>
  )
}

export const useCart = () => useContext(CartContext);
