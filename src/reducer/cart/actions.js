const ADD = "add";
const CLEAR = "clear";
const REMOVE = "remove";
const OPEN_CART = "open_cart";
const CLOSE_CART = "close_cart";

export {
  ADD,
  CLEAR,
  REMOVE,
  OPEN_CART,
  CLOSE_CART
};
