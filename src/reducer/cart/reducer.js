import { ADD, CLEAR, REMOVE, CLOSE_CART, OPEN_CART } from './actions';

const reducer = (state, action) => {
  switch (action.type) {
    case ADD:

      if (!state.products[action.product.id]) {
        state.products[action.product.id] = [];
      }

      state.products[action.product.id].push(action.product);
      state.total = state.total + action.product.price;
      state.cartOpen = true;

      localStorage.setItem( 'products', JSON.stringify(state) );

      return { ...state };
    case REMOVE:

      if (state.products[action.product.id] && state.products[action.product.id].length > 0) {
        state.total = (state.total).toFixed(2) - (action.product.price).toFixed(2);
        state.products[action.product.id].splice(-1);
      }

      localStorage.setItem( 'products', JSON.stringify(state) );

      return { ...state };
    case CLEAR:
      return { products: {}, total: 0 };

    case OPEN_CART:
      state.cartOpen = true;

      localStorage.setItem( 'products', JSON.stringify(state) );

      return { ...state };

    case CLOSE_CART:
      state.cartOpen = false;

      localStorage.setItem( 'products', JSON.stringify(state) );

      return { ...state };

    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
}

export default reducer;
