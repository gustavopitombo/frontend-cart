import React from 'react';
import ProductsPage from './components/pages/products';
import { CartProvider } from "./reducer/cart/context";

import './assets/css/normalize.css';
import './assets/sass/index.scss';

function App() {
  return (
    <CartProvider>
      <ProductsPage></ProductsPage>
    </CartProvider>
  );
}

export default App;
