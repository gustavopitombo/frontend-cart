import React from 'react';
import { useCart } from "../../../reducer/cart/context";

import CartHeader from '../../elements/cart/cart-header';
import CartFooter from '../../elements/cart/cart-footer';
import CartList from '../../elements/cart/cart-list';

import './style.scss';

function Cart() {
  const { state } = useCart();

  return (
    <div className={`cart ${(state.cartOpen ? '-open' : '')}`}>
      <div className="cart__content">
        <CartHeader></CartHeader>
        <CartList></CartList>
        <CartFooter></CartFooter>
      </div>
    </div>
  );
}

export default Cart;
