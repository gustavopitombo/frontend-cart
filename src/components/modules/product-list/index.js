import React, { useState, useEffect } from 'react';
import ProductItem from '../../elements/products/product-item';
import fetchProducts from '../../../services/product';

import './style.scss';

function ProductList() {
  const [productsData, setProducts] = useState(null);

  useEffect(() => {
    fetchProducts()
      .then(data => setProducts(data))
      .catch(error => setProducts({ error: true }));
  }, []);

  if (!productsData) {
    return 'Carregando ...';
  }

  if (productsData.error) {
    return 'Erro ao obter produtos.';
  }

  const { products } = productsData;

  return (
    <div className="product-list">
      {
        products.map(item => <ProductItem {...item} />)
      }
    </div>
  );
}

export default ProductList;
