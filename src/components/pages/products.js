import React from 'react';
import Cart from '../modules/cart';

import ProductList from '../modules/product-list';

function ProductsPage() {
  return (
    <>
      <ProductList></ProductList>
      <Cart></Cart>
    </>
    );
}

export default ProductsPage;
