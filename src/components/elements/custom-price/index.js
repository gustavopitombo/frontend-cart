import React from 'react';

const customPrice = (currencyFormat, priceValue) => {
  const priceString = priceValue.toFixed(2).toString();
  const price = priceString.split('.');

  return (<span className='price'>
    <span className='currency'>{currencyFormat} </span>
    <span className='value'>{price[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}</span>,<span className='decimal'>{price[1]}</span>
    </span>
  );
}

export default customPrice;
