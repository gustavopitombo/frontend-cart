import React from 'react';
import { useCart } from "../../../../reducer/cart/context";
import { ADD } from "../../../../reducer/cart/actions";
import customPrice from '../../custom-price';

import './style.scss';

function ProductItem(props) {
  const { dispatch } = useCart();
  const productPrice = customPrice(props.currencyFormat, props.price);
  const installmentsPrice = customPrice(props.currencyFormat, (props.price / props.installments));
  const imageUrl = `/statics/images/${props.id}.jpg`;

  return (
    <div className="product-item">
      <div className="product-item__content">
        <img src={imageUrl} />
        <p className="product-item__title">{props.title}</p>
        <p className="product-item__price">{productPrice}</p>
        {
          !!props.installments &&
          <p className="product-item__installment">ou {props.installments}x {installmentsPrice}</p>
        }
        <button onClick={() => dispatch({ type: ADD, product: { ...props } })}>+</button>
      </div>
    </div>
  );
}

export default ProductItem;
