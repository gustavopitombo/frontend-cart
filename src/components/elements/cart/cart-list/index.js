import React from 'react';
import { useCart } from "../../../../reducer/cart/context";
import CartItem from '../cart-item';

function CartList() {
  const { state } = useCart();

  if (!state.products) {
    return;
  }

  return (
    <div className="cart-list">
      {Object.keys(state.products).map(function(key) {
        if (state.products[key].length > 0) {
          return <CartItem info={state.products[key][0]} total={state.products[key].length} />
        }
      })}
    </div>
  );
}

export default CartList;
