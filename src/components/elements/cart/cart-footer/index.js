import React from 'react';
import { useCart } from "../../../../reducer/cart/context";
import customPrice from '../../custom-price';

import './style.scss';

function CartFooter() {
  const { state } = useCart();
  const cartPrice = customPrice('R$', state.total);
  const installmentPrice = customPrice('R$', (state.total / 10));

  return (
    <div className="cart-footer">
      <div className="cart-footer__content">
        <p className="cart-footer__title">Subtotal:</p>
        <div>
          <p className="cart-footer__total">{cartPrice}</p>
          <p className="cart-footer__installment">ou em até 10x de {installmentPrice}</p>
        </div>
      </div>
      <button className="cart-footer__buy">Comprar</button>
    </div>
  );
}

export default CartFooter;
