import React from 'react';
import { CLOSE_CART } from "../../../../reducer/cart/actions";
import { useCart } from "../../../../reducer/cart/context";

import './style.scss';

const getTotalProducts = (products) => {
  let total = 0;

  Object.keys(products).forEach(key =>
    total += products[key].length
  );

  return total;
}

function CartHeader() {
  const { dispatch, state } = useCart();

  return (
    <div className="cart-header">
      <button className='cart-header__close' onClick={(e) => dispatch({ type: CLOSE_CART })}>X</button>
      <div className='cart-header__icon'>
        <span>{getTotalProducts(state.products)}</span>
      </div>
      <p className="cart-header__title">Sacola</p>
    </div>
  );
}

export default CartHeader;
