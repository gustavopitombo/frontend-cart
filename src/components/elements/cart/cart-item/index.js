import React from 'react';
import { useCart } from "../../../../reducer/cart/context";
import { REMOVE } from "../../../../reducer/cart/actions";
import customPrice from '../../custom-price';

import './style.scss';

function CartItem(props) {
  const { dispatch } = useCart();
  const { id, availableSizes, currencyFormat, title, price, style } = props.info;
  const productPrice = customPrice(currencyFormat, price);
  const imageUrl = `/statics/images/${id}.jpg`;

  return (
    <div className="cart-item">
      <div className="cart-item__thumbnail">
        <img src={imageUrl} />
      </div>
      <div className="cart-item__info">
        <p className="cart-item__title">{title}</p>
        <p className="cart-item__sizes">{availableSizes.join(',')} | {style}</p>
        <p className="cart-item__total">Quantidade: {props.total}</p>
      </div>
      <div className="cart-item__actions">
        <button className="cart-item__bt-remove" onClick={() => dispatch({ type: REMOVE, product: props.info })}>X</button>
        <p className="cart-item__price">{productPrice}</p>
      </div>
    </div>
  );
}

export default CartItem;
