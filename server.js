var express = require('express');
var cors = require('cors');
var app = express();
var products = require('./products.json');

app.use(cors());

app.get('/products', function(req, res) {
  res.json(products);
});

app.listen(3333);
